package jf.free.org.ssca.demo.fact;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
@EnableDiscoveryClient
@EnableDubbo(scanBasePackages = "jf.free.org.ssca.demo")
public class SwarmScaDemoFactApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwarmScaDemoFactApplication.class, args);
	}

}
