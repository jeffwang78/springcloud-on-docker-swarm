package jf.free.org.ssca.demo.fact;

import jf.free.org.ssca.demo.services.Factorial;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.web.bind.annotation.RestController;

@DubboService
@RestController
@SuppressWarnings ("unused")
public class FactorialImpl implements Factorial
{
	@Override
	public int factorial (int n)
	{
		n = Math.abs (n) ;
		int r = n ;
		for (int i = 2 ; i < n ; i ++)
			r = r * i ;
		return r ;
	}
}
