package jf.free.org.ssca.demo.vars;

import jf.free.org.ssca.demo.services.Variables;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@RestController
@SuppressWarnings ("unused")
public class VariablesImpl implements Variables
{
	/**
	 * A simple mapped storage of Variables.
	 */
	protected static ConcurrentHashMap<String, Integer> map = new ConcurrentHashMap<> () ;

	@Override
	public int getVar (String varName)
	{
		map.putIfAbsent (varName, 0) ;
		return map.getOrDefault (varName, 0);
	}

	@Override
	public int setVar (String varName, int varValue)
	{
		map.put (varName, varValue) ;
		return varValue ;
	}

	@Override
	public Map<String, Integer> getVars ()
	{
		return map;
	}
}
