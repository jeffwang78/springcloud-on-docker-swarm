package jf.free.org.ssca.demo.sentinel.nacosconfig;

import com.alibaba.nacos.api.config.ConfigFactory;
import com.alibaba.nacos.api.config.ConfigService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * The config class for Nacos client building (with connecting params). <br>
 *
 * Properties in spring application.properties with prefix:
 * <code>sentinel.nacos.config</code>
 * <ul>
 *     <li>server-addr: default value is localhost:8848 </li>
 *     <li>namespace: default value is "public" </li>
 * </ul>
 */
@Configuration
public class NacosConfig
{
	@Value ("${sentinel.nacos.config.server-addr:localhost}")
	private String serverAddr = "localhost" ;
	@Value ("${sentinel.nacos.config.namespace:public}")
	private String namespace = "public" ;


	/**
	 * Create a nacos client bean.
	 * @return ConfigService by server-addr and namespace.
	 * @throws Exception exception during creation.
	 */
	@Bean
	public ConfigService nacosConfigService() throws Exception {
		final Properties props = new Properties () ;

		props.setProperty ("serverAddr", serverAddr) ;
		props.setProperty ("namespace", namespace) ;

		return ConfigFactory.createConfigService(props);
	}
}
