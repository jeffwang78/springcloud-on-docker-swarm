package jf.free.org.ssca.demo.sentinel.nacosconfig;

import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.RuleEntity;
import com.alibaba.fastjson.JSON;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


/**
 * This class intercept set Xxx Rules method of
 * {@link com.alibaba.csp.sentinel.dashboard.client.SentinelApiClient} and publish rules to Nacos.
 *
 * @see #group 
 * @see #buildDataId(String, String)
 */
@Component
@Aspect
@SuppressWarnings ("unused")
public class NacosInterceptorPublisher
{

	private static final String FLOW_RULE_TYPE = "flow";
	private static final String DEGRADE_RULE_TYPE = "degrade";
	private static final String SYSTEM_RULE_TYPE = "system";
	private static final String AUTHORITY_TYPE = "authority";
	private static final String GATEWAY_FLOW_TYPE = "gw-flow";
	private static final String PARAM_FLOW_TYPE = "param-flow";

	private final Logger logger = LoggerFactory.getLogger(NacosInterceptorPublisher.class);

	@Autowired
	private ConfigService nacosConfigService ;

	/**
	 * The group id of nacos config, default value is "SENTINEL_DEFAULT".
	 */
	@Value ("${sentinel.nacos.config.group:SENTINEL_DEFAULT}")
	protected String group = "SENTINEL_DEFAULT" ;


	/**
	 * Intercept SentinelApiClient.setFlowRuleOfMachineAsync.
	 * @param point the JoinPoint.
	 * @return result of point.
	 */
	@Around ("execution (* com.alibaba.csp.sentinel.dashboard.client.SentinelApiClient.setFlowRuleOfMachineAsync (..))")
	public Object interceptFlowRule (ProceedingJoinPoint point)
	{
		String type = FLOW_RULE_TYPE ;
		return wrap (type, point) ;
	}

	/**
	 * Intercept SentinelApiClient.setDegradeRuleOfMachine.
	 * @param point the JoinPoint.
	 * @return result of point.
	 */
	@Around ("execution (* com.alibaba.csp.sentinel.dashboard.client.SentinelApiClient.setDegradeRuleOfMachine (..))")
	public Object interceptDegradeRule (ProceedingJoinPoint point)
	{
		String type = DEGRADE_RULE_TYPE ;
		return wrap (type, point) ;
	}

	/**
	 * Intercept SentinelApiClient.setAuthorityRuleOfMachine.
	 * @param point the JoinPoint.
	 * @return result of point.
	 */
	@Around ("execution (* com.alibaba.csp.sentinel.dashboard.client.SentinelApiClient.setAuthorityRuleOfMachine (..))")
	public Object interceptAuthorityRule (ProceedingJoinPoint point)
	{
		String type = AUTHORITY_TYPE ;
		return wrap (type, point) ;
	}
	/**
	 * Intercept SentinelApiClient.setSystemRuleOfMachine.
	 * @param point the JoinPoint.
	 * @return result of point.
	 */
	@Around ("execution (* com.alibaba.csp.sentinel.dashboard.client.SentinelApiClient.setSystemRuleOfMachine (..))")
	public Object interceptSystemRule (ProceedingJoinPoint point)
	{
		String type = SYSTEM_RULE_TYPE ;
		return wrap (type, point) ;
	}
	/**
	 * Intercept SentinelApiClient.modifyGatewayFlowRules.
	 * @param point the JoinPoint.
	 * @return result of point.
	 */
	@Around ("execution (* com.alibaba.csp.sentinel.dashboard.client.SentinelApiClient.modifyGatewayFlowRules (..))")
	public Object interceptGatewayFlowRule (ProceedingJoinPoint point)
	{
		String type = GATEWAY_FLOW_TYPE ;
		return wrap (type, point) ;
	}
	/**
	 * Intercept SentinelApiClient.setParamFlowRuleOfMachine.
	 * @param point the JoinPoint.
	 * @return result of point.
	 */
	@Around ("execution (* com.alibaba.csp.sentinel.dashboard.client.SentinelApiClient.setParamFlowRuleOfMachine (..))")
	public Object interceptParamFlowRule (ProceedingJoinPoint point)
	{
		String type = PARAM_FLOW_TYPE ;
		return wrap (type, point) ;
	}

	/**
	 * Wrapper of {@link  #publishRules(String, String, List)}.
	 * @param type the rule type, one of: flow, system, authority, degrade,param-flow.
	 * @param point the point.
	 * @return result of point.
	 */
	protected Object wrap (String type, ProceedingJoinPoint point)
	{
		logger.info (" intercept start [" + point.getSignature () + "]");

		Object [] args = point.getArgs () ;
		String app = (String) args [0];

		List<? extends RuleEntity> entities = (List<? extends RuleEntity>) args [3];

//		call to publish rule
		try
		{
			boolean result = publishRules (app, type, entities);

			if (! result)
				throw new RuntimeException ("publish rules to nacos error: return false.");
		}
		catch (NacosException e)
		{
			logger.error ("publish rules to nacos error.");
			throw new RuntimeException ("publish rules to nacos error.", e);
		}


		try
		{
			Object ret = point.proceed ();

			logger.info (" intercept end [" + point.getSignature () + "]");

			return ret ;
		}
		catch (Throwable t)
		{
			logger.error ("CutPoint proceed Error.", t);
			throw new RuntimeException (t) ;
		}

	}

	/**
	 * Publish config to nacos server.
	 * @param app name of application.
	 * @param type type of rules.
	 * @param entities rules list.
	 * @return true if published successfully.
	 * @throws NacosException thrown by Nacos.
	 */
	public boolean publishRules (String app,
		String type, List<? extends RuleEntity> entities) throws NacosException
	{
//		make nacos dataId ;
		String dataId = buildDataId (app, type);

//		copy from sentinel client code .
		String data = JSON.toJSONString(
				entities.stream().map(r -> r.toRule())
						.collect(Collectors.toList()));

		logger.info ("app  : {}", app) ;
		logger.info ("type : {}", type) ;
		logger.info ("Rules: {}", data) ;


		return nacosConfigService.publishConfig (dataId, this.group, data, "json") ;
	}

	/**
	 * Build a config data id: {app}-{type}-rules.
	 * @param app name of application
	 * @param type type of rules
	 * @return data id
	 */
	public String buildDataId (String app, String type)
	{
		return app + "-" + type + "-rules" ;
	}
}
