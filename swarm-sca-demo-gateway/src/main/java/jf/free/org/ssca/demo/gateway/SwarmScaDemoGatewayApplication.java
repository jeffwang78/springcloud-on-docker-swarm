package jf.free.org.ssca.demo.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class SwarmScaDemoGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwarmScaDemoGatewayApplication.class, args);
	}

}
