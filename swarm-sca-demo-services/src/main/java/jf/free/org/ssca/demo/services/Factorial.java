package jf.free.org.ssca.demo.services;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Service interface for Factorial function.
 */
@FeignClient ("service-fact")
public interface Factorial
{
	/**
	 * Perform factorial function
	 * @param n input number
	 * @return n!
	 */
	@GetMapping ("/fact/{n}")
	int factorial (@PathVariable  int n) ;

}
