package jf.free.org.ssca.demo.services;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Map;

/**
 * The Var service interface for feign stub.
 */
@FeignClient("service-vars")
public interface Variables
{
	/**
	 * HTTP Get for retrieve var value.
	 * @param varName name of var
	 * @return value of var
	 */
	@GetMapping ("/var/{varName}")
	int getVar (@PathVariable @NonNull String varName);

	/**
	 * HTTP Post for update var value.
	 * @param varName name of var
	 * @return value of var
	 */
	@PostMapping ("/var/{varName}/{varValue}")
	@SuppressWarnings("UnusedReturnValue")
	int setVar (@PathVariable @NonNull String varName,
				@PathVariable int varValue);


	/**
	 * HTTP Get for list all Vars.
	 * @return the name-value map of all Vars.
	 */
	@GetMapping ("/var/list")
	@SuppressWarnings ("unused")
	Map<String, Integer> getVars () ;
}
