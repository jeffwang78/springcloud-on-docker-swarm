package jf.free.org.ssca.demo.services;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * The Feign service stub.
 */
@FeignClient("service-calc")
public interface Calculator
{
	/**
	 * Return v1 + V2 value. If v1 is Var, update result to var.
	 * @param v1 number or var name.
	 * @param v2 number or var name.
	 * @return sum of v1, v2.
	 */
	@GetMapping ("/plus/{v1}/{v2}")
	@SuppressWarnings ("unused")
	int plus (
			@PathVariable String v1,
			@PathVariable String v2) ;

}
