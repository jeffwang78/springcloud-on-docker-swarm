# App common docker file v1
# 1. 运行一个alpine操作系统：run ... alpine  
FROM alpine 
# 2. 安装JDK: apk add openjdk8
RUN apk add openjdk8 curl
# 3. 安装jar: docker cp .jar /app/
ARG app_name
ARG app_version
ENV APP_JAR_NAME=$app_name-$app_version.jar
COPY $app_name/target/$APP_JAR_NAME /app/

# 声明暴露 8080端口
ENV JVM_OPTS=
ENV SERVER_PORT=8080 
EXPOSE $SERVER_PORT 

ENV APP_HEALTH_URI=actuator/health/liveness

HEALTHCHECK --interval=15s --timeout=5s --start-period=10s --retries=5 \
  CMD curl -s --fail http://localhost:$SERVER_PORT/$APP_HEALTH_URI || exit 1

# 4. 进入/app/目录：cd /app/
WORKDIR /app/

# 5. 启动jar: java -jar ...
ENTRYPOINT java $JVM_OPTS -jar $APP_JAR_NAME --server.port=$SERVER_PORT

