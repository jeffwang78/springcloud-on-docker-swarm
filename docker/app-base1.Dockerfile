# App common docker file v1
# 1. 运行一个alpine操作系统：run ... alpine  
FROM alpine 
# 2. 安装JDK: apk add openjdk8
RUN apk add openjdk8
# 3. 安装jar: docker cp .jar /app/
ARG app_name
ARG app_version
ENV APP_JAR_NAME=$app_name-$app_version.jar
COPY $app_name/target/$APP_JAR_NAME /app/

# 声明暴露 8080端口
EXPOSE 8080/tcp
# 4. 进入/app/目录：cd /app/
WORKDIR /app/
# 5. 启动jar: java -jar ...
ENTRYPOINT java -jar $APP_JAR_NAME
