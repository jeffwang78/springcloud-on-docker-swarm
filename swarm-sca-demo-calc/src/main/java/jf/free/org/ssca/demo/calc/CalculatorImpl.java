package jf.free.org.ssca.demo.calc;

import jf.free.org.ssca.demo.services.Calculator;
import jf.free.org.ssca.demo.services.Factorial;
import jf.free.org.ssca.demo.services.Variables;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SuppressWarnings ("unused")
@RestController
public class CalculatorImpl implements Calculator
{
	/**
	 * Variable feign client .
	 */
	@Autowired
	protected Variables vars ;

	@Override
	public int plus (
			@PathVariable String v1,
			@PathVariable String v2)
	{
		int n1,n2 ;

		String [] varName = {null} ;
		// Check v1, v2 is a Vars ;

		n1 = getVal (v1, varName) ;
		n2 = getVal (v2, null) ;

		int r = n1 + n2  ;

		if (! StringUtils.isEmpty (varName [0]))
		{
			vars.setVar (varName [0], r) ;
		}
		return r ;
	}

	/**
	 * Dubbo service stub.
	 */
	@DubboReference
	protected Factorial fact ;

	/**
	 * Also use a Feign stub.
	 */
	@Autowired
	protected Factorial factFeign ;

	/**
	 * return arg value by var or factorial.
	 * @param arg arg, one of number, var name (with !).
	 * @param varName return varName [0] if arg contains a var names.
	 * @return number value of arg.
	 */
	protected int getVal (String arg, String [] varName)
	{
		boolean isFact = false ;
		if (arg.endsWith ("!"))
		{
			isFact = true ;
			arg = arg.substring (0, arg.length () - 1).trim ();
		}
		int val ;
		if (StringUtils.isNumeric (arg))
		{
			val = Integer.parseInt (arg);
		}
		else
		{
			val = vars.getVar (arg) ;
			if (varName != null && varName.length > 0)
				varName [0] = arg ;
		}

		if (isFact)
		{
//			Test For feign call and dubbo call.
//			for feign, it locates service only for service address:port
//			dubbo service register only as service metadata.
			factFeign.factorial (val) ;

			val = fact.factorial (val) ;
		}

		return val ;
	}
}
