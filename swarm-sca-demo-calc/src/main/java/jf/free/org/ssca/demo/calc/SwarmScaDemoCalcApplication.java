package jf.free.org.ssca.demo.calc;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
		(basePackages = {"jf.free.org.ssca.demo.services"})
@EnableDiscoveryClient
@EnableDubbo
public class SwarmScaDemoCalcApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwarmScaDemoCalcApplication.class, args);
	}

}
